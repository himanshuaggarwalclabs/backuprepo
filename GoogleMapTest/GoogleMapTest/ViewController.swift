//
//  ViewController.swift
//  GoogleMapTest
//
//  Created by Click Labs on 3/19/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit
 var mapView: GMSMapView!
// LongPressed Coordinates
var LPlatitude  = CLLocationDegrees()
var LPlongitude = CLLocationDegrees()
// CurrentLocation Coordinates
var CLlatitude  = CLLocationDegrees()
var CLlongitude = CLLocationDegrees()


let screenSize: CGRect = UIScreen.mainScreen().bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
class ViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
  //30.739830, 76.774180
  //var gmaps: GMSMapView?
  //var marker = GMSMarker()
  //var manager = CLLocationManager()
    var marker = GMSMarker()
   //var marker1 = GMSMarker()
   var mapMarker = GMSMarker()
  var mapView: GMSMapView!
  var manager = CLLocationManager()
  var locationFixAchieved : Bool = false
  
  
  //required init(coder aDecoder: NSCoder) {
  //  super.init(coder: aDecoder)
  //}


  override func viewDidLoad() {
    super.viewDidLoad()
    
self.showRoute()
    //self.mapView.delegate = self
    
    manager.delegate = self
    
    manager.desiredAccuracy = kCLLocationAccuracyBest
    manager.requestWhenInUseAuthorization()
    manager.startUpdatingLocation()
    
    var camera = GMSCameraPosition.cameraWithLatitude(30.719057,
      longitude:76.810401, zoom:15)
    mapView = GMSMapView.mapWithFrame(CGRectMake(0, 0, screenWidth, screenHeight), camera:camera)
    mapView.delegate=self
    mapView.myLocationEnabled = true
    mapView.settings.compassButton = true
    mapView.settings.myLocationButton = true
    self.mapView.setMinZoom(10, maxZoom: 25)
    
    // showRoute()
    
    //self.view = mapView
    
    //var marker = GMSMarker()
    marker.position = CLLocationCoordinate2DMake(30.719057, 76.810401)
    marker.title = "Click Labs"
    marker.snippet = "My Office"
    marker.map = mapView
    
    self.view.addSubview(mapView)
    
    
   /* var path = GMSMutablePath()
    path.addLatitude(30.719057, longitude:76.810401) // Sydney
    path.addLatitude(30.739830, longitude:76.774180) // Fiji
   // path.addLatitude(21.291, longitude:-157.821) // Hawaii
    //path.addLatitude(37.423, longitude:-122.091) // Mountain View
    
    var polyline = GMSPolyline(path: path)
    polyline.strokeColor = UIColor.blueColor()
    polyline.strokeWidth = 5.0
    polyline.map = mapView
    
    //self.view = mapView*/
  }
  
  
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    if (locationFixAchieved == false) {
      locationFixAchieved = true
      var locationArray = locations as NSArray
      var locationObj = locationArray.lastObject as CLLocation
      var coord = locationObj.coordinate
      
      println(coord.latitude)
      CLlatitude = coord.latitude
      println(coord.longitude)
      CLlongitude = coord.longitude
    }
   // println("location\(locations)")
    println("Latitude = \(CLlatitude)")
    println("Longitude =\(CLlongitude) ")
  }

  /*
  func mapView(mapView: GMSMapView!, didLongPressAtCoordinate coordinate: CLLocationCoordinate2D) {
    
    
    
    GMSGeocoder().reverseGeocodeCoordinate(coordinate){
      (response, error) in
      if let address = response?.firstResult() {
        
        let r = GMSReverseGeocodeResponse()
        
        self.marker1.position = coordinate
        self.marker1.appearAnimation = kGMSMarkerAnimationPop
        self.marker1.map = self.mapView
        self.marker1.title = "\(address.thoroughfare)\n\(address.subLocality)"
        self.marker1.snippet = address.administrativeArea + ", " + address.country + "(\(address.postalCode))"
        var cameraUpdate = GMSCameraUpdate.setTarget(coordinate)
        self.mapView.animateWithCameraUpdate(cameraUpdate)
        
        
        
      }
    }
  }
*/
  

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  
  
  //This function dectects a long press on the map and places a marker at the coordinates of the long press.
  func mapView(mapView: GMSMapView!, didLongPressAtCoordinate coordinate: CLLocationCoordinate2D) {
    //----------------
    GMSGeocoder().reverseGeocodeCoordinate(coordinate){
      (response, error) in
      if let address = response?.firstResult() {
        
        let r = GMSReverseGeocodeResponse()
        //---------------*
        //Set variable to latitude of didLongPressAtCoordinate
        var latitude = coordinate.latitude
        
        LPlatitude = latitude
        
        
        //Set variable to longitude of didLongPressAtCoordinate
        var longitude = coordinate.longitude
        
        LPlongitude = longitude
        
        //Feed position to mapMarker
        self.mapMarker.position = CLLocationCoordinate2DMake(latitude, longitude)
        
        //Define attributes of the mapMarker.
        self.mapMarker.icon = UIImage(named: "mapmarkericon")
        
        //Set icon anchor point
        self.mapMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        
        //Enable animation on mapMarker
        self.mapMarker.appearAnimation = kGMSMarkerAnimationPop
        //-------------
        self.mapMarker.title = "\(address.thoroughfare)\n\(address.subLocality)"
        self.mapMarker.snippet = address.administrativeArea + ", " + address.country + "(\(address.postalCode))"
        var cameraUpdate = GMSCameraUpdate.setTarget(coordinate)
        self.mapView.animateWithCameraUpdate(cameraUpdate)
        
        //------------*
        //Display the mapMarker on the mapView.self.
        self.mapMarker.map = mapView
        
        self.drawCircle(coordinate)
        
      }
    }
  }
  
  
  func drawCircle(position: CLLocationCoordinate2D) {
    
    //var latitude = position.latitude
    //var longitude = position.longitude
    //var circleCenter = CLLocationCoordinate2DMake(latitude, longitude)
    var circle = GMSCircle(position: position, radius: 300)
    circle.strokeColor = UIColor(red: 0, green: 0, blue: 0.35, alpha: 0.05)
    circle.fillColor = UIColor(red: 0, green: 0, blue: 0.35, alpha: 0.05)
    circle.map = mapView
    
    ployLine()
  }
  
  func ployLine(){
    
    var path = GMSMutablePath()
    path.addLatitude(30.719057, longitude:76.810401) // Sydney
    path.addLatitude(LPlatitude, longitude:LPlongitude) // Fiji
    // path.addLatitude(21.291, longitude:-157.821) // Hawaii
    //path.addLatitude(37.423, longitude:-122.091) // Mountain View
    
    var polyline = GMSPolyline(path: path)
    polyline.strokeColor = UIColor.cyanColor()
    polyline.strokeWidth = 5.0
    polyline.map = mapView

  }
  
  func showRoute(){
    
    var sourceLat = CLLocationDegrees()
    var sourceLong = CLLocationDegrees()
    var destinationLat = CLLocationDegrees()
    var destinationLong = CLLocationDegrees()
    
    sourceLat = CLlatitude
    sourceLong = CLlongitude
    destinationLat = LPlatitude
    destinationLong = LPlongitude

    var urlString = NSURL(string: "https://www.google.co.in")
    
    let task = NSURLSession.sharedSession().dataTaskWithURL(urlString!) {
      (data,response,error) in

      var urlContent = NSString(data: data, encoding:NSUTF8StringEncoding)
      println(urlContent)
    }
    

}

}
