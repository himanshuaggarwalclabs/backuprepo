//
//  Header.h
//  GoogleMapTest
//
//  Created by Click Labs on 3/19/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//


#import <GoogleMaps/GoogleMaps.h>
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
//#import <ViewConroller.swift>

@interface MDDirectionService : NSObject
-(void)showRoute:(id)routeDict;
-(void)showRoute;
-(NSMutableArray *)decodePolyLine:(NSString *)encodedStr;
@end

/*
-(void)showRoute:(id)routeDict {
  NSString *pointStr = [[[[routeDict objectForKey:@"routes"] objectAtIndex:0] objectForKey:@"overview_polyline"] objectForKey:@"points"];
  
  float max_long = 0.0;
  float min_long = 0.0;
  
  float max_lat = 0.0;
  float min_lat = 0.0;
  
  NSMutableArray *routeArr = [self decodePolyLine:pointStr];
  CLLocationCoordinate2D commuterLotCoords[[routeArr count]];
  CLLocation *loc;
  if ([routeArr count]) {
    loc = [routeArr objectAtIndex:0];
    max_long = loc.coordinate.longitude;
    min_long = loc.coordinate.longitude;
    
    max_lat = loc.coordinate.latitude;
    min_lat = loc.coordinate.latitude;
  }
  
  for (int i=0; i<[routeArr count]; i++) {
    CLLocation *loc = [routeArr objectAtIndex:i];
    commuterLotCoords[i] = loc.coordinate;
    
    if (loc.coordinate.latitude > max_lat) {
      max_lat = loc.coordinate.latitude;
    }
    if (loc.coordinate.latitude < min_lat) {
      min_lat = loc.coordinate.latitude;
    }
    
    
    if (loc.coordinate.longitude > max_long) {
      max_long = loc.coordinate.longitude;
    }
    if (loc.coordinate.longitude < min_long) {
      min_long = loc.coordinate.longitude;
    }
    
    
    
    
  }
  
  MKPolyline *overflowRoutePolygon = [MKPolyline polylineWithCoordinates:commuterLotCoords count:[routeArr count]];
  
  [mapView addOverlay:overflowRoutePolygon];
  
  //NSLog(@"%f %f %f %f",min_lat,max_lat,min_long,max_long);
  if ( max_lat == 0.0 || min_lat == 0.0 || max_long == 0.0 || min_long == 0.0 ) {
  } else {
    //calculate center of map
    float center_long = (max_long + min_long) / 2;
    float center_lat = (max_lat + min_lat) / 2;
    
    //calculate deltas
    float deltaLat = max_lat - min_lat + .00032;
    float deltaLong = max_long - min_long + .00032;
    //NSLog(@"%f %f %f %f",center_lat,center_long,deltaLat,deltaLong);
    
    //create new region and set map
    CLLocationCoordinate2D cordinate;
    cordinate.latitude = center_lat;
    cordinate.longitude = center_long;
    MKCoordinateSpan span = MKCoordinateSpanMake(deltaLat, deltaLong);
    MKCoordinateRegion region = {cordinate, span};
    [mapView setRegion:region];
  }
}

-(void)showRoute
{
  
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  float sourceLat = 0.00;
  float sourceLong = 0.00;
  float destinationLat = 0.00;
  float destinationLong = 0.00;
  id record = [routeArray objectAtIndex:selectedIndex];
  sourceLat = [[[[record objectForKey:@"To"] objectForKey:@"Latitude"] objectForKey:@"text"] floatValue];
  sourceLong =  [[[[record objectForKey:@"To"] objectForKey:@"Longitude"] objectForKey:@"text"] floatValue];
  
  destinationLat = [[[[record objectForKey:@"From"] objectForKey:@"Latitude"] objectForKey:@"text"] floatValue];
  destinationLong =  [[[[record objectForKey:@"From"] objectForKey:@"Longitude"] objectForKey:@"text"] floatValue];
  
  if ( sourceLat == 0.00 || sourceLong == 0.00 || destinationLat == 0.00 || destinationLong == 0.00 ) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Direction Error"
                                                    message: @"One of your destination is not valid."
                                                   delegate: self
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    [alert show];
    [alert release];
    return;
  }
  
  NSString *urlStr = [NSString stringWithFormat: @"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",
                      sourceLat, sourceLong,
                      destinationLat,destinationLong];
  NSLog(@"urlStr : %@",urlStr);
  NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]];
  //NSLog(@"direction response : %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
  
  JSONDecoder *jsonKitDecoder = [JSONDecoder decoder];
  NSMutableDictionary *routeDic = [[jsonKitDecoder objectWithData:data] copy];
  if ( ![[[routeDic objectForKey:@"status"] uppercaseString] isEqualToString:@"OK"] ) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Direction Error"
                                                    message: @"Not able to find direction from your default location."
                                                   delegate: self
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    [alert show];
    [alert release];
    [pool release];
    return;
  }
  
  [self performSelectorOnMainThread:@selector(showRoute:) withObject:routeDic waitUntilDone:YES];
  
  [pool release];
  return;
}

-(NSMutableArray *)decodePolyLine:(NSString *)encodedStr

{
  
  NSMutableString *encoded = [[NSMutableString alloc] initWithCapacity:[encodedStr length]];
  [encoded appendString:encodedStr];
  [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\" options:NSLiteralSearch range:NSMakeRange(0, [encoded length])];
  NSInteger len = [encoded length];
  NSInteger index = 0;
  NSMutableArray *array = [[[NSMutableArray alloc] init] autorelease];
  NSInteger lat = 0;
  NSInteger lng = 0;
  while (index < len) {
    NSInteger b;
    NSInteger shift = 0;
    NSInteger result = 0;
    do {
      b = [encoded characterAtIndex:index++] - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
    lat += dlat;
    shift = 0;
    result = 0;
    do {
      b = [encoded characterAtIndex:index++] - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
    lng += dlng;
    NSNumber *latitute = [[[NSNumber alloc] initWithFloat:lat * 1e-5] autorelease];
    NSNumber *longitute = [[[NSNumber alloc] initWithFloat:lng * 1e-5] autorelease];
    CLLocation *loc = [[[CLLocation alloc] initWithLatitude:[latitute floatValue] longitude:[longitute floatValue]] autorelease];
    
    [array addObject:loc];
  }
  [encoded release];
  return array;
}

//pragma mark MKMapKitDelegate


- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation


{
  HAnnotation *ann = (HAnnotation *)annotation;
  static NSString *AnnotationViewID = @"annotationViewID";
  
  HAnnotationView *annotationView = (HAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
  if (annotationView == nil)
  {
    annotationView = [[[HAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID type:ann.type] autorelease];
    
  }
  annotationView.type = ann.type;
  annotationView.annotation = annotation;
  [annotationView setNeedsDisplay];
  return annotationView;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views

{
  
  
  MKAnnotationView *aV;
  for (aV in views) {
    MKAnnotationView* annotationView = aV;
    annotationView.canShowCallout = NO;
  }
}



- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id < MKOverlay >)overlay

{
  
  
  if( [overlay isKindOfClass:[MKPolyline class]] ) {
    MKPolylineView *view = [[MKPolylineView alloc] initWithOverlay:overlay];
    view.lineWidth=2;
    view.strokeColor=[UIColor blueColor];
    view.fillColor=[[UIColor blueColor] colorWithAlphaComponent:0.5];
    return [view autorelease];
  } else {
    MKPolygonView *view = [[MKPolygonView alloc] initWithOverlay:overlay];
    view.lineWidth=2;
    view.strokeColor=[UIColor yellowColor];
    view.fillColor=[[UIColor yellowColor] colorWithAlphaComponent:0.3];
    return [view autorelease];
  }
  return nil;
}
*/



